"""
Credit:
Cliff Zhang (Website: cliffzhang.net)
Vedant Gupta (Website: vedantgupta.me)
"""


import sys
import random

def settings():
    global env 
    global keys
    global screensize, gridsize
    global coverDelayThreshold
    env = getEnvVar()
    keys = list(env.keys())
    screensize = int(env["screensize"])
    gridsize = int(env["grid"])
    coverDelayThreshold = int(env["cover_delay"])
    size(screensize, screensize)

def setup():
    background(240)
    setupState()
    setupFont()
    setupDimensions()
    setupBtns()
    setupDogImageDict()
    setupGrid()
    setupCoverImages()
    displayBlocks()
    displayBtns()

def draw():
    coverAfterDelay()
    checkWin()
    updateTimer()

def mouseReleased():
    global indexClicked, timerDisplay
    duringDelay = False
    try:
        coverDelayTime
    except NameError:
        pass
    else:
        if millis() - coverDelayTime < coverDelayThreshold:
            duringDelay = True
    indexClicked = None
    if duringDelay == False:
        for i in range(gridsize**2):
            if ((mouseX > gridBoundaries[i][0][0]) and (mouseX < gridBoundaries[i][1][0])) and ((mouseY > gridBoundaries[i][0][1]) and (mouseY < gridBoundaries[i][1][1])):
                indexClicked = i
                if timerActive == False:
                    startTimer()
    if (mouseX > startBtnX and mouseX < startBtnX + startBtnW) and (mouseY > btnY and mouseY < btnY + btnH):
        startTimer()
    elif (mouseX > stopBtnX and mouseX < stopBtnX + stopBtnW) and (mouseY > btnY and mouseY < btnY + btnH):
        stopTimer()
    elif (mouseX > restartBtnX and mouseX < restartBtnX + restartBtnW) and (mouseY > btnY and mouseY < btnY + btnH):
        restartTimer()
        resetBoard()
    if indexClicked == None:
        return
    else:
        updateState()
        updateImages()
        checkImages()

def getEnvVar():
    try:
        file = open("env.txt", "r")
        fileContent = file.readlines()
        env = {}
        for i in range (len(fileContent)):
            pair = fileContent[i].strip("/n").split()
            env[pair[0]] = pair[1]
        return env
    except:
        print("Environment file not found. Exiting")
        sys.exit()

def displayBtns():
    image(btnImageDict["start_image"], startBtnX, btnY, startBtnW, btnH)
    image(btnImageDict["stop_image"], stopBtnX, btnY, stopBtnW, btnH)
    image(btnImageDict["restart_image"], restartBtnX, btnY, restartBtnW, btnH)

def displayBlocks():
    for i in range(gridsize**2):
        image(questionmark, gridBoundaries[i][0][0], gridBoundaries[i][0][1], blockW, blockH)
  
def setupFont():
    fontSize = screensize * 0.05
    font = createFont("AppleSDGothicNeo-UltraLight-40.vlw", fontSize);
    textFont(font)

def setupState():
    global stateArr
    # State >>> -1:Covered 0:Uncovered 1:Removed
    stateArr = [[-1 for i in range(gridsize)] for i in range(gridsize)]
  
def setupDimensions():
    global btnH, btnY, startBtnX, startBtnW
    global stopBtnX, stopBtnW
    global restartBtnX, restartBtnW
    global boardX, boardY, boardH
    global blockH, blockW
    global marginLeft
    global timerX, timerY, timerH
    global timeX, timeY
    menuH = screensize * 0.125
    btnH = menuH / 2.5
    btnY = menuH * 0.3
    startBtnX = screensize * 0.15
    startBtnW = btnH / 7 * 26
    stopBtnX = screensize * 0.4
    stopBtnW = btnH / 34 * 101
    restartBtnX = screensize * 0.6
    restartBtnW = btnH / 34 * 173
    boardX = 0
    boardY = menuH
    boardH = screensize * 0.75
    blockH = blockW = boardH / gridsize
    marginLeft = (screensize - blockW * gridsize) / 2
    timerX = 0
    timerY = boardY + boardH
    timerH = screensize * 0.125
    timeX = marginLeft
    timeY = timerY + timerH / 1.65

def setupBtns():
    global btnImageDict, timerActive, timerDisplay
    btnImageDict = {aKey: loadImage(env[aKey]) for aKey in keys if "start_image" in aKey or "stop_image" in aKey or "restart_image" in aKey}
    timerActive = False
    timerDisplay = 0

def setupDogImageDict():
    global dogImagePathDict
    dogImagePathDict = {aKey: env[aKey] for aKey in keys if "dog_image" in aKey}
  
def setupGrid():
    global grid2d, gridBoundaries, won
    grid2d = []
    grid1d = []
    won = False
    for i in range(len(dogImagePathDict)):
        grid1d.extend([i, i])
    random.shuffle(grid1d)
    for i in range(0, gridsize**2, gridsize):
        grid2d.append(grid1d[i : i + gridsize])
    gridBoundaries = []
    for i in range(gridsize):
        for j in range(gridsize):
            gridBoundaries.append([[marginLeft + blockW * j, blockH * i + boardY], [marginLeft + blockW * (j + 1), blockH * (i + 1) + boardY]])
    
def setupCoverImages():
    global questionmark, checkmark
    questionmark = loadImage(env["questionmark_image"])
    checkmark = loadImage(env["checkmark_image"])
    
def updateState():
    global stateArr
    global row, col
    row = indexClicked // gridsize
    col = indexClicked % gridsize
    if stateArr[row][col] == -1:
        stateArr[row][col] = 0
    
def updateImages():
    currentDogImage = loadImage(dogImagePathDict["dog_image" + str(grid2d[row][col])])
    if stateArr[row][col] != 1:
        image(currentDogImage, gridBoundaries[indexClicked][0][0], gridBoundaries[indexClicked][0][1], blockW, blockH)
  
def checkImages():
    global stateArr
    global coverDelayTime
    global toCover1X, toCover1Y, toCover2X, toCover2Y
    numFlipped = sum(stateRow.count(0) for stateRow in stateArr)
    if numFlipped == 2:
        twoIndices = []
        for i in range(gridsize):
            for j, x in enumerate(stateArr[i]):
                if x == 0:
                    twoIndices.append([i, j])
        if grid2d[twoIndices[0][0]][twoIndices[0][1]] == grid2d[twoIndices[1][0]][twoIndices[1][1]]:
            stateArr[twoIndices[0][0]][twoIndices[0][1]] = stateArr[twoIndices[1][0]][twoIndices[1][1]] = 1
            image(checkmark, gridBoundaries[twoIndices[0][0]*gridsize + twoIndices[0][1]][0][0], gridBoundaries[twoIndices[0][0]*gridsize + twoIndices[0][1]][0][1], blockW, blockH)
            image(checkmark, gridBoundaries[twoIndices[1][0]*gridsize + twoIndices[1][1]][0][0], gridBoundaries[twoIndices[1][0]*gridsize + twoIndices[1][1]][0][1], blockW, blockH)
        else:
            coverDelayTime = millis()
            stateArr[twoIndices[0][0]][twoIndices[0][1]] = stateArr[twoIndices[1][0]][twoIndices[1][1]] = -1
            toCover1X = gridBoundaries[twoIndices[0][0]*gridsize + twoIndices[0][1]][0][0]
            toCover1Y = gridBoundaries[twoIndices[0][0]*gridsize + twoIndices[0][1]][0][1]
            toCover2X = gridBoundaries[twoIndices[1][0]*gridsize + twoIndices[1][1]][0][0]
            toCover2Y = gridBoundaries[twoIndices[1][0]*gridsize + twoIndices[1][1]][0][1]

def coverAfterDelay():
    global coverDelayTime
    try:
        coverDelayTime
    except NameError:
        pass
    else:
        if millis() - coverDelayTime > coverDelayThreshold:
            image(questionmark, toCover1X, toCover1Y, blockW, blockH)
            image(questionmark, toCover2X, toCover2Y, blockW, blockH)
            del coverDelayTime

def startTimer():
    global timerActive, timerTime, timerDisplay
    timerActive = True
    timerTime = millis()

def stopTimer():
    global timerActive
    timerActive = False
  
def updateTimer():
    global timerTime, timerDisplay
    if timerActive:
        if millis() > timerTime + 1000:
            timerDisplay += 1
            timerTime = millis()
        fill(240)
        noStroke()
        rect(timerX, timerY, screensize, timerH)
        fill(0)
        text("Time Elapsed: " + str(timerDisplay) + "s", timeX, timeY)
        
def restartTimer():
    global timerDisplay
    timerDisplay = 0
    startTimer()

def resetBoard():
    setupState()
    setupDogImageDict()
    setupGrid()
    setupCoverImages()
    displayBlocks()

def checkWin():
    hasWon = True
    for state in stateArr:
        if sum(state) != gridsize:
            hasWon = False
            break
    won = hasWon
    if won:
        stopTimer()
